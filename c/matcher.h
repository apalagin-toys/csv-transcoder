#ifndef MATCHER_H_INCLUDED
#define MATCHER_H_INCLUDED

#include "hashmap.h"

typedef struct matcher matcher_t;
struct matcher {
    map_t *map;
};

matcher_t *matcher_get(char *file, char *match_column, char *read_column);
void matcher_match(matcher_t *matcher, char *value, char *output);

#endif // MATCHER_H_INCLUDED
