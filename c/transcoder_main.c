#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "transcoder_main.h"
#include "types.h"
#include "config.h"
#include "csvwriter.h"
#include "utils.h"
#include "timestamp.h"
#include "actions.h"
#include "matcher.h"
#include "expression.h"

void print_header(csvwriter_t *writer, config_t *config);

void read_field_value(CsvRow *row, const CsvRow *header, char *field, char *value);
int is_match_configured(output_t *out);
parsed_value_t *parse_field_value(input_t *input, char *input_val);
void transcode_value(parsed_value_t *parsed_value, output_t *output, char *out_str);

char *get_alias(input_t *input);
parsed_value_t *evaluate_expression(char *expression, map_t *vars);
int is_filtered(CsvRow *row, const CsvRow *header, config_t *config);

int transcoder( struct cmdargs *args, int argc, char *argv[], int optind )
{
    // cmd parameters
    char *conf_file = args->config;
    char *in_file = argv[optind];
    char out_file[1024] = "output.csv";
    if (args->output != NULL) {
        strcpy(out_file, args->output);
    }

    int i;
    char field_value[255];

//    char *conf_file = "c:\\Users\\apalagin\\Desktop\\gitlab.com\\apalagin-toys\\csv-transcoder\\c\\test\\sample1.config.xml";
////    char *in_file = "c:\\Users\\apalagin\\Desktop\\gitlab.com\\apalagin-toys\\csv-transcoder\\c\\test\\sample1.csv", *out_file = "output.csv";
//    //char *in_file = "c:\\Users\\apalagin\\Desktop\\gitlab.com\\apalagin-toys\\csv-transcoder\\samples\\1987.csv", *out_file = "output.csv";
//    char *in_file = "c:\\Users\\apalagin\\Desktop\\gitlab.com\\apalagin-toys\\csv-transcoder\\samples\\dbm.log", *out_file = "output.csv";

    puts("Parsing configuration...");
    config_t *config = parse_xml_config(conf_file);
    if (config == NULL) {
        puts("Failed to parse configuration!");
        return 1;
    }

    puts("Creating output file...");
    csvwriter_t *writer = csvwriter_open(out_file, ',');
    if (writer == NULL) {
        puts("Failed to open output file!");
        return 1;
    }

    // Print header based on configuration
    print_header(writer, config);

    puts("Opening input file...");
    CsvParser *parser = CsvParser_new(in_file, ",", 1);
    CsvRow *row;

    puts("Parsing header...");
    const CsvRow *header = CsvParser_getHeader(parser);
    if (header == NULL) {
        printf("%s\n", CsvParser_getErrorMessage(parser));
        return 1;
    }

    input_t *input;
    output_t *output;
    parsed_value_t *value_to_transcode;
    char out_str[1024];

    printf("Parsing data...");
    while ((row = CsvParser_getRow(parser))) {
        if (is_filtered(row, header, config)) {
            CsvParser_destroy_row(row);
            continue;
        }

        rule_t *rule = NULL;
        for (rule = config->rules_head; rule != NULL; rule = rule->next) {
            output = rule->output;
            // If no expression's defined only one input is assumed
            if (strlen(output->expression) == 0) {
                input = rule->inputs_head;
                read_field_value(row, header, input->field, field_value);
                value_to_transcode = parse_field_value(input, field_value);
            } else {
                // Collecting variables into map
                map_t *vars = map_new(10);
                parsed_value_t *tmp;
                for (input = rule->inputs_head; input != NULL; input = input->next) {
                    read_field_value(row, header, input->field, field_value);
                    tmp = parse_field_value(input, field_value);
                    map_put(vars, get_alias(input), tmp);
                }
                // Evaluating expression with variables
                value_to_transcode = evaluate_expression(output->expression, vars);
                map_destroy(vars);
            }

            memset(out_str, '\0', sizeof(out_str));
            transcode_value(value_to_transcode, output, out_str);
            free(value_to_transcode);
            csvwriter_write(writer, out_str);
        }

        csvwriter_eol(writer);
        CsvParser_destroy_row(row);
    }

    CsvParser_destroy(parser);
    csvwriter_close(writer);

    puts("Done!");
    return 0;
}

void print_header(csvwriter_t *writer, config_t *config)
{
    rule_t *rule;
    for (rule = config->rules_head; rule != NULL; rule = rule->next) {
        csvwriter_write(writer, rule->output->field);
    }
    csvwriter_eol(writer);
}

parsed_value_t *parse_field_value(input_t *input, char *input_val)
{
    parsed_value_t *result = malloc(sizeof(parsed_value_t));

    switch (input->type) {
        case LONG:
            result->type = LONG;
            parse_long(input_val, &(result->as.integer));
            break;

        case DATE:
            result->type = DATE;
            parse_timestamp(input_val, input->format, &(result->as.date));
            break;

        case DOUBLE:
            result->type = DOUBLE;
            parse_number(input_val, &(result->as.number));
            break;

        default:
            result->type = TEXT;
            strcpy(result->as.text, input_val);
    }

    return result;
}

void transcode_value(parsed_value_t *parsed_value, output_t *output, char *out_str)
{
    // TODO: This looks ugly and supports only sample type conversions
    switch (output->type) {
        case LONG:
            switch (parsed_value->type) {
                default:
                    sprintf(out_str, "%ld", parsed_value->as.integer);
            }
            break;

        case DATE:
            switch (parsed_value->type) {
                default:
                    format_timestamp(&(parsed_value->as.date), output->format, out_str);
            }
            break;

        case DOUBLE:
            switch (parsed_value->type) {
                default:
                    if (strlen(output->format) == 0)
                        strcpy(output->format, "%.6f");
                    sprintf(out_str, output->format, parsed_value->as.number);
            }
            break;

        default:
            switch (parsed_value->type) {
                case TEXT:
                default:
                    strcpy(out_str, parsed_value->as.text);
            }
    }


    // Matching
    if (is_match_configured(output)) {
        matcher_t *matcher = matcher_get(output->match_file, output->match_column, output->read_column);
        matcher_match(matcher, out_str, out_str);
    }

    // Apply action if defined
    if (strlen(output->action) > 0) {
        apply_action(output->action, out_str);
    }
}

int is_match_configured(output_t *out)
{
    return strlen(out->match_file) > 0
           && strlen(out->match_column) > 0
           && strlen(out->read_column) > 0;
}

char *get_alias(input_t *input)
{
    if (strlen(input->alias) > 0)
        return input->alias;
    return input->field;
}

int is_filtered(CsvRow *row, const CsvRow *header, config_t *config)
{
    filter_t *filter = config->filter;
    if (filter == NULL)
        return 0;

    char value[1024];
    map_t *vars = map_new(10);

    input_t *input = filter->inputs_head;
    while (input != NULL) {
        read_field_value(row, header, input->field, value);
        parsed_value_t *tmp = parse_field_value(input, value);
        map_put(vars, get_alias(input), tmp);

        input = input->next;
    }

    // For filtering we expect conditional expressions returning TRUE or FALSE
    // Which we expect to get via integer field
    parsed_value_t *result = evaluate_expression(filter->expression, vars);
    int bool_flag = result->as.integer;

    free(result);
    map_destroy(vars);

    return !bool_flag;
}
