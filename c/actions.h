#ifndef ACTIONS_H_INCLUDED
#define ACTIONS_H_INCLUDED

int apply_action(const char action[], char *str);

#endif // ACTIONS_H_INCLUDED
