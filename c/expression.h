//
// Created by apalagin on 6/18/2017.
//

#ifndef CSV_TRANSCODER_EXPRESSION_H
#define CSV_TRANSCODER_EXPRESSION_H

#include "hashmap.h"
#include "types.h"

parsed_value_t *evaluate_expression(char *expression, map_t *vars);

#endif //CSV_TRANSCODER_EXPRESSION_H
