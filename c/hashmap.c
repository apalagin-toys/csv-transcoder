#include "hashmap.h"
#include <string.h>

unsigned int hash(const char key[]);
map_entry_t *find_entry(map_t *map, size_t idx, char key[]);
map_entry_t *new_entry(char key[], void *data);
size_t key_to_index(map_t *map, char key[]);

map_t *map_new(size_t capacity)
{
    map_t *map = malloc(sizeof(map_t));
    map->capacity = capacity;

    map->size = 0;
    map->buckets = calloc(capacity, sizeof(map_entry_t *));

    int i;
    for (i = 0; i < capacity; i++) {
        map->buckets[i] = NULL;
    }

    return map;
}

// CHECK Memory management? Who should free data?
size_t map_put(map_t *map, char key[], void *data)
{
    size_t idx = key_to_index(map, key);
    map_entry_t *entry = find_entry(map, idx, key);
    if (entry == NULL) {
        entry = new_entry(key, data);
        if (map->buckets[idx] != NULL) entry->next = map->buckets[idx];
        map->buckets[idx] = entry;
    } else {
        free(entry->data);
        entry->data = data;
    }

    return idx;
}

void *map_get(map_t *map, char key[])
{
    size_t idx = key_to_index(map, key);
    map_entry_t *e = find_entry(map, idx, key);
    if (e != NULL) return e->data;
    return NULL;
}

void *map_delete(map_t *map, char key[])
{
    size_t idx = key_to_index(map, key);
    map_entry_t *e = find_entry(map, idx, key);
    if (e == NULL) return 0;

    if (e->prev != NULL) {
        e->prev->next = e->next;
    } else if (e->next != NULL) {
        map->buckets[idx] = e->next;
    } else {
        map->buckets[idx] = NULL;
    }

    void *data_ptr = e->data;
    free(e);

    return data_ptr;
}

int map_contains_key(map_t *map, char key[])
{
    size_t idx = key_to_index(map, key);
    return find_entry(map, idx, key) != NULL;
}

// PRIVATE
// ==================================================================

size_t key_to_index(map_t *map, char key[])
{
    return hash(key) % map->capacity;
}

// Ly hash
unsigned int hash(const char key[])
{
    unsigned int hash = 0;

	for(; *key; key++) {
        hash = (hash * 1664525) + (unsigned char) (*key) + 1013904223;
    }

	return hash;
}

map_entry_t *new_entry(char key[], void *data)
{
    map_entry_t *entry = malloc(sizeof(map_entry_t));
    strcpy(entry->key, key);
    entry->data = data;
    entry->prev = NULL;
    entry->next = NULL;
    return entry;
}

map_entry_t *find_entry(map_t *map, size_t idx, char key[])
{
    map_entry_t *entry = map->buckets[idx];
    if (entry == NULL) return NULL;

    while (strcmp(entry->key, key) != 0) {
        if (entry->next == NULL) return NULL;
        entry = entry->next;
    }

    return entry;
}

void   map_destroy(map_t *map)
{
    int i;
    map_entry_t *entry, *tmp;

    for (i = 0; i < map->capacity; i++) {
        if (map->buckets[i] != NULL) {
            entry = map->buckets[i];
            while (entry != NULL) {
                tmp = entry;
                entry = entry->next;

                if (tmp->data != NULL) free(tmp->data);
                free(tmp);
            }
        }
    }

    free(map->buckets);
    free(map);
}

map_iterator_t *map_iterator(map_t *map)
{
    int i;
    map_entry_t *entry, *copy;
    map_iterator_t *result = malloc(sizeof(map_iterator_t));
    result->head = NULL;
    result->size = 0;

    // Building list of entries
    for (i = 0; i < map->capacity; i++) {
        entry = map->buckets[i];
        while (entry != NULL) {
            // Create copy of entry to change "next" link
            copy = malloc(sizeof(map_entry_t));
            strcpy(copy->key, entry->key);
            copy->data = entry->data;
            copy->next = NULL;
            copy->prev = NULL;

            // Add it to list
            if (result->head == NULL) {
                result->head = copy;
            } else {
                copy->next = result->head;
                result->head = copy;
            }

            result->size++;
            entry = entry->next;
        }
    }

    return result;
}

void map_iterator_free(map_iterator_t *it) {
    map_entry_t *tmp, *e = it->head;
    while (e != NULL) {
        tmp = e;
        e = tmp->next;
        free(tmp);
    }

    free(it);
}
