#include <stdio.h>
#include <string.h>

#include "utils.h"

int parse_long(char *str, long *l)
{
    return sscanf(str, "%ld", l);
}

int parse_number(char *str, double *d)
{
    return sscanf(str, "%lf", d);
}

void read_field_value(CsvRow *row, const CsvRow *header, char *field, char *field_value)
{
    int i, len = CsvParser_getNumFields(header);
    const char **headers = CsvParser_getFields(header);

    for (i = 0; i < len; i++) {
        if (strstr(headers[i], field))
            break;
    }

    const char **values = CsvParser_getFields(row);
    // TODO: Add check for row size
    strcpy(field_value, values[i]);
}
