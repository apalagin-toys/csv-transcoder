#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "lib/ezxml/ezxml.h"

filter_t *parse_filter_tag(ezxml_t filter_tag);
output_t *parse_output_tag(ezxml_t output_tag);

type_t type_of(const char *str) {
    if (strcmp(str, LONG_STR) == 0)
        return LONG;

    if (strcmp(str, DOUBLE_STR) == 0)
        return DOUBLE;

    if (strcmp(str, DATE_STR) == 0)
        return DATE;

    return TEXT;
}

config_t* parse_xml_config(char *filename)
{
    rule_t *last_rule;
    config_t *config = (config_t *) malloc(sizeof(config_t));
    config->rules_head = NULL;

    ezxml_t root = ezxml_parse_file(filename);
    if (root == NULL)
        return NULL;

    ezxml_t filter_tag = ezxml_child(root, "filter");
    if (filter_tag != NULL) {
        filter_t *filter = parse_filter_tag(filter_tag);
        config->filter = filter;
    }

    ezxml_t rules = ezxml_child(root, "rules");
    if (rules == NULL)
        return NULL;

    ezxml_t rule_tag, input_tag, output_tag;
    for (rule_tag = ezxml_child(rules, "rule"); rule_tag; rule_tag = rule_tag->next) {
        last_rule = malloc(sizeof(rule_t));
        last_rule->inputs_head = NULL;
        last_rule->inputs_len = 0;
        last_rule->next = NULL;

        // Expected multiple inputs
        int i = 0;
        for (input_tag = ezxml_child(rule_tag, "input"); input_tag; input_tag = input_tag->next) {
            input_t *input = malloc(sizeof(input_t));
            input->next = NULL;
            memset(input->field, '\0', 255);
            memset(input->alias, '\0', 255);
            memset(input->format, '\0', 255);
            input->type = TEXT;

            strcpy(input->field, ezxml_attr(input_tag, "field"));
            if (ezxml_attr(input_tag, "var") != NULL)
                strcpy(input->alias, ezxml_attr(input_tag, "var"));

            input->type = type_of(ezxml_attr(input_tag, "type"));
            if (input->type == DATE) {
                strcpy(input->format, ezxml_attr(input_tag, "format"));
            }

            // Attaching to the list of inputs
            if (last_rule->inputs_head == NULL) {
                last_rule->inputs_head = input;
            } else {
                input_t *in = last_rule->inputs_head;
                while (in->next != NULL) in = in->next;
                in->next = input;
            }
        }

        last_rule->inputs_len = i;

        // But only single output
        output_tag = ezxml_child(rule_tag, "output");
        output_t *output = parse_output_tag(output_tag);
        last_rule->output = output;

        // Attaching to the tail of the list
        if (config->rules_head == NULL) {
            config->rules_head = last_rule;
        } else {
            rule_t *rule = config->rules_head;
            while (rule->next != NULL) rule = rule->next;
            rule->next = last_rule;
        }
    }

    ezxml_free(root);
    return config;
}

output_t *parse_output_tag(ezxml_t output_tag)
{
    char *attr_val;
    output_t *output = malloc(sizeof(output_t));

    memset(output->field,  '\0', 255);
    memset(output->format, '\0', 255);
    memset(output->action, '\0', 255);
    output->type = TEXT;

    memset(output->match_file,   '\0', 255);
    memset(output->match_column, '\0', 255);
    memset(output->read_column,  '\0', 255);

    memset(output->expression,  '\0', 255);

    strcpy(output->field, ezxml_attr(output_tag, "field"));
    output->type = type_of(ezxml_attr(output_tag, "type"));

    if (output->type == DATE)
        strcpy(output->format, ezxml_attr(output_tag, "format"));

    attr_val = ezxml_attr(output_tag, "action");
    if (attr_val != NULL)
        strcpy(output->action, attr_val);

    attr_val = ezxml_attr(output_tag, "match_file");
    if (attr_val != NULL)
        strcpy(output->match_file, attr_val);

    attr_val = ezxml_attr(output_tag, "match_column");
    if (attr_val != NULL)
        strcpy(output->match_column, attr_val);

    attr_val = ezxml_attr(output_tag, "read_column");
    if (attr_val != NULL)
        strcpy(output->read_column, attr_val);

    attr_val = ezxml_attr(output_tag, "expression");
    if (attr_val != NULL)
        strcpy(output->expression, attr_val);

    return output;
}

filter_t *parse_filter_tag(ezxml_t filter_tag)
{
    filter_t *result = malloc(sizeof(filter_t));
    result->inputs_head = NULL;

    const char *expr = ezxml_attr(filter_tag, "expression");
    strcpy(result->expression, expr);

    ezxml_t input_tag;
    for (input_tag = ezxml_child(filter_tag, "input"); input_tag; input_tag = input_tag->next) {
        input_t *input = malloc(sizeof(input_t));
        input->next = NULL;
        memset(input->field, '\0', 255);
        memset(input->alias, '\0', 255);
        memset(input->format, '\0', 255);
        input->type = TEXT;

        strcpy(input->field, ezxml_attr(input_tag, "field"));
        strcpy(input->alias, ezxml_attr(input_tag, "var"));

        input->type = type_of(ezxml_attr(input_tag, "type"));
        if (input->type == DATE) {
            strcpy(input->format, ezxml_attr(input_tag, "format"));
        }

        if (result->inputs_head == NULL) {
            result->inputs_head = input;
        } else {
            input_t *head = result->inputs_head;
            result->inputs_head = input;
            input->next = head;
        }
    }

    return result;
}

