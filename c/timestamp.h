#ifndef TIMESTAMP_H_INCLUDED
#define TIMESTAMP_H_INCLUDED

#include "types.h"
#include <time.h>

void parse_timestamp  (char *str, char *format, timestamp_t *tm);
void format_timestamp (timestamp_t *tm, char *format, char *out);

#endif // TIMESTAMP_H_INCLUDED
