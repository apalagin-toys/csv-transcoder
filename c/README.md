C version
===============================

Initially developed on Windows 10 using MingGW-w64-4.2.0 + Code::Blocks IDE.
Then project was migrated to CLion which uses CMake for building.  Anyway
implementation is bounded by Windows C runtime capabilities.

Performance
-------------------------------

| platform | gcc --version | Sample          | Time      | CPU   | RAM  | 
| -------- | ------------- | --------------- | --------- | ----- | ---- |
| Windows  | 5.3.0         | dbm.log (352M)  | 0m52.627s | < 19% | 9M   |
| Windows  | 5.3.0         | 1987.csv (122M) | 2m31.778s | < 19% | 2.5M |

Runtime and Libraries
-------------------------------

* GCC 5.x
* CMake
* CSV parser from https://sourceforge.net/projects/cccsvparser/
* XML parser from http://ezxml.sourceforge.net/
* MD5 implementation from http://openwall.info/wiki/people/solar/software/public-domain-source-code/md5
* JavaScript engine from Duktape.org
* CLI parser generator from https://code.google.com/archive/p/cgener/ 

Build instructions
-------------------------------

For Windows:
- Install prerequisites: MinGW and CMake
- Generate Makefile: cmake -G "MinGW Makefiles"
- Compile: mingw-make

Implementation notes and TODOs
------------------------------

C is not for pussies! Production-ready code would be 2x time longer than current one. Because I'm lazy and do not devote
enough time my code is unsafe and sloppy: in production every malloc/free call result must be checked, array indexes 
must be controlled, buffers should be reallocated to prevent overflown etc.

MinGW runtime does not provide argp.h so command line parsing is custom via Google's cgener.

Also MinGW does not support parsing timestamps! Note that GNU libc has methods for that, but not Windows C Runtime. 
I didn't find suitable library for C (though StackOverflow had few suggestions). Instead I implemented a pretty dumb 
yet straightforward parser to support example formats. That means C version is limited comparing to Java/Groovy.

Implementing linked lists and hashmaps/hashtables is fun, but is also super error-prone! I have no doubts that my API is ugly.
And it also took a while to fix numerous memory leaks and segmentation faults.

I didn't find any examples how to evaluate expressions in Duktape in a way similar to Java/Groovy. Current implementation 
creates a mini script with global variables and feeds it to Duktape.

Duktape engine is a pure interpretation w/o JIT compilation and seems slower then Groovy/Java. But memory footprint is
incredibly small.
