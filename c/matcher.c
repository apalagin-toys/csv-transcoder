#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "matcher.h"
#include "csvparser.h"
#include "utils.h"

static map_t *matchers = NULL;

matcher_t *matcher_new(char *file, char *match_column, char *read_column);
void gen_matcher_key(char *file, char *match_col, char *read_col, char *key);

matcher_t *matcher_get(char *file, char *match_column, char *read_column)
{
    char key[1024];
    gen_matcher_key(file, match_column, read_column, key);

    if (matchers == NULL) {
        matchers = map_new(16);
    }

    matcher_t *matcher = map_get(matchers, key);
    if (matcher == NULL) {
        matcher = matcher_new(file, match_column, read_column);
        map_put(matchers, key, matcher);
    }

    return matcher;
}

void matcher_match(matcher_t *matcher, char *value, char *output)
{
    char *result;
    map_t *map;

    map = matcher->map;
    result = map_get(map, value);
    if (result == NULL) {
        memset(output, '\0', 1);
    } else {
        strcpy(output, result);
    }
}

matcher_t *matcher_new(char *file, char *match_column, char *read_column)
{
    matcher_t *matcher = malloc(sizeof(matcher_t));
    map_t *map = map_new(1024);
    matcher->map = map;

    CsvParser *csvparser = CsvParser_new(file, ",", 1);
    CsvRow *row;

    const CsvRow *header = CsvParser_getHeader(csvparser);

    char key[255], value[255];
    while ((row = CsvParser_getRow(csvparser))) {
        read_field_value(row, header, match_column, key);
        read_field_value(row, header, read_column, value);

        if (strlen(key) > 0 && strlen(value) > 0) {
            // "value" is on stack while map must point to heap ptr
            char *data = malloc(strlen(value) + 1);
            strcpy(data, value);
            map_put(map, key, data);
        }

        CsvParser_destroy_row(row);
    }

    CsvParser_destroy(csvparser);

    return matcher;
}

void gen_matcher_key(char *file, char *match_col, char *read_col, char *key)
{
    char *result = malloc(1024 * sizeof(char));
    memset(result, '\0', 1024);

    result = strcat(result, file);
    result = strcat(result, match_col);
    result = strcat(result, read_col);

    strcpy(key, result);
    free(result);
}
