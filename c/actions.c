#include "actions.h"
#include "lib/md5/md5.h"

#include <string.h>

void to_hexstr(unsigned char *src, char *dst, int len);
int md5_hash(char *str);

int apply_action(const char action[], char *str)
{
    if (strcmp(action, "hash") == 0) {
        return md5_hash(str);
    }

    return -1;
}

int md5_hash(char *str)
{
    unsigned char result[16];
    MD5_CTX ctx;

    MD5_Init(&ctx);
    MD5_Update(&ctx, str, strlen(str));
    MD5_Final(result, &ctx);

    to_hexstr(result, str, 16);

    return strlen(str);
}

static const char hex_digits[17] = "0123456789abcdef";

void to_hexstr(unsigned char *src, char *dst, int len)
{
    int i, j;
    char result[2 * 16 + 1] = { '\0' };

    for (i = 0, j = 0; i < len; i++)
    {
        result[j++] = hex_digits[ (src[i] & 0xF0) >> 4 ];
        result[j++] = hex_digits[ (src[i] & 0x0F) ];
    }

    strcpy(dst, result);
}

