#include "timestamp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int   is_pattern_symbol (char c);
int   substr_by_pattern (char buf[], char *str, char *format, char pattern);
void  parse_unix_ts     (char *str, char *format, timestamp_t *tm);
char *to_cformat        (char *format);

/* Java style timestamp parser (limited).
   Special characters supported:
   yyyy - 4-digit year
   yy  - 2 digit year
   MM  - 2-digit month
   dd  - 2-digit day
   HH  - 2-digit hour
   mm  - 2-digit minutes
   ss  - 2-digit seconds
   S   - Unix timestamp
   SS  - Unix timestamp in milliseconds
   SSS - Unix timestamp in nanoseconds
*/
const char patterns[] = "yMdHms";

void parse_timestamp(char *str, char *format, timestamp_t *tm)
{
    // Implement analog of strptime for Windows CRT?
    int i, j;
    int len = strlen(format);
    char buf[5];

    memset(tm, 0, sizeof(timestamp_t));

    if (len < 5) {
        parse_unix_ts(str, format, tm);
        return;
    }

    for (i = 0; i < len; i++) {
        if (is_pattern_symbol(format[i])) {
            if (format[i] == 'y') {
                // year
                i += substr_by_pattern(buf, &str[i], &format[i], 'y');
                // TODO: Add correction for 2 digit year
                tm->tm_year = atoi(buf) - 1900;
            } else if (format[i] == 'M') {
                // month
                i += substr_by_pattern(buf, &str[i], &format[i], 'M');
                tm->tm_mon = atoi(buf);
            } else if (format[i] == 'd') {
                // day
                i += substr_by_pattern(buf, &str[i], &format[i], 'd');
                tm->tm_mday = atoi(buf);
            } else if (format[i] == 'H') {
                // hour
                i += substr_by_pattern(buf, &str[i], &format[i], 'H');
                tm->tm_hour = atoi(buf);
            } else if (format[i] == 'm') {
                // minutes
                i += substr_by_pattern(buf, &str[i], &format[i], 'm');
                tm->tm_min = atoi(buf);
            } else if (format[i] == 's') {
                // hour
                i += substr_by_pattern(buf, &str[i], &format[i], 's');
                tm->tm_sec = atoi(buf);
            }
        }
    }
}

void format_timestamp(timestamp_t *tm, char *format, char *out)
{
    // Reusing strftime (exists in Windows CRT) by converting Java format to C-style
    char *cformat = to_cformat(format);
    strftime(out, 255, cformat, tm);
    free(cformat);
}

int is_pattern_symbol(char c)
{
    int i = 0;
    for (; i < 6; i++) {
        if (patterns[i] == c) return 1;
    }

    return 0;
}

void parse_unix_ts(char *str, char *format, timestamp_t *tm)
{
    time_t epoch_sec = atoll(str);

    if (strcmp(format, "SSS") == 0) {
        epoch_sec /= 1000000;
    } else if (strcmp(format, "SS") == 0) {
        epoch_sec /= 1000;
    }

    timestamp_t *tmp = gmtime(&epoch_sec);
    *tm = *tmp;
    //free(tmp);
}

int substr_by_pattern(char buf[], char *str, char *format, char pattern)
{
    int i = 0;
    memset(buf, '\0', 5);

    while (*(format++) == pattern) {
        buf[i] = *(str++);
        i++;
    }

    return i;
}

char *to_cformat(char format[])
{
    char *result = malloc(255);
    memset(result, '\0', 255);

    int i, j, cnt;
    char ch;

    i = j = cnt = 0;
    while (format[i] != '\0') {
        ch = format[i];
        if (ch == 'y') {
            if (++cnt == 4) {
                result[j++] = '%';
                result[j++] = 'Y';

                cnt = 0;
            }
        } else if (ch == 'M') {
            if (++cnt == 2) {
                result[j++] = '%';
                result[j++] = 'm';

                cnt = 0;
            }
        } else if (ch == 'd') {
            if (++cnt == 2) {
                result[j++] = '%';
                result[j++] = 'd';

                cnt = 0;
            }
        } else if (ch == 'H') {
            if (++cnt == 2) {
                result[j++] = '%';
                result[j++] = 'H';

                cnt = 0;
            }
        } else if (ch == 'm') {
            if (++cnt == 2) {
                result[j++] = '%';
                result[j++] = 'M';

                cnt = 0;
            }
        } else if (ch == 's') {
            if (++cnt == 2) {
                result[j++] = '%';
                result[j++] = 'S';

                cnt = 0;
            }
        } else {
            result[j++] = ch;
        }

        i++;
    }

    result[i] = '\0';
    return result;
}
