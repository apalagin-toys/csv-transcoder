#include "csvwriter.h"
#include <stdlib.h>
#include <string.h>

csvwriter_t * csvwriter_open(char *filename, char delimiter)
{
    csvwriter_t *ctx = malloc(sizeof(csvwriter_t));
    ctx->is_line_empty = 1;
    memset(ctx->buffer, 0, sizeof(ctx->buffer));
    ctx->file = fopen(filename, "w");
    if (ctx->file == NULL) {
        free(ctx);
        return NULL;
    }

    return ctx;
}

int csvwriter_write(csvwriter_t *ctx, char *value)
{
    if (ctx->is_line_empty) {
        ctx->is_line_empty = 0;
    } else {
        strcat(ctx->buffer, ",");
    }

    strcat(ctx->buffer, value);
}

int csvwriter_eol(csvwriter_t *ctx)
{
    int result = fprintf(ctx->file, "%s\n", ctx->buffer);
    ctx->is_line_empty = 1;
    memset(ctx->buffer, 0, sizeof(ctx->buffer));
    return result;
}

int csvwriter_close(csvwriter_t *ctx)
{
    fclose(ctx->file);
    free(ctx);

    return 0;
}
