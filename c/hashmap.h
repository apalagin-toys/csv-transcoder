#ifndef HASHMAP_H_INCLUDED
#define HASHMAP_H_INCLUDED

#include <stdlib.h>

#define DEALLOCATE_ENTRIES 1

typedef struct map_entry map_entry_t;
struct map_entry {
    char key[255];
    void *data;
    map_entry_t *next;
    map_entry_t *prev;
};

typedef struct map map_t;
struct map {
    map_entry_t **buckets;
    size_t capacity;
    size_t size;
};

typedef struct map_iterator map_iterator_t;
struct map_iterator {
    map_entry_t *head;
    size_t size;
};

map_t *map_new(size_t capacity);
size_t map_put(map_t *m, char key[], void *data);
void *map_get(map_t *m, char key[]);
void *map_delete(map_t *m, char key[]);
int map_contains_key(map_t *m, char key[]);
void map_destroy(map_t *m);
map_iterator_t *map_iterator(map_t *m);
void map_iterator_free(map_iterator_t *it);

#endif // HASHMAP_H_INCLUDED
