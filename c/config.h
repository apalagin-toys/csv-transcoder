#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#include "types.h"

#define LONG_STR "long"
#define DOUBLE_STR "double"
#define DATE_STR "date"
#define TEXT_STR "string"

config_t * parse_xml_config(char *filename);

#endif // CONFIG_H_INCLUDED
