#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <csvparser.h>
#include "types.h"

int parse_long( char *str, long *l );
int parse_number( char *str, double *l );
void read_field_value( CsvRow *row, const CsvRow *header, char *field, char *field_value );

#endif // UTILS_H_INCLUDED
