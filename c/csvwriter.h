#ifndef CSVWRITER_H_INCLUDED
#define CSVWRITER_H_INCLUDED

#include <stdio.h>

typedef struct {
    // TODO: That should be dynamic with realloc
    char buffer[2*1024+1];
    int is_line_empty;
    FILE *file;
} csvwriter_t;

csvwriter_t* csvwriter_open(char *file, char separator);
int csvwriter_write(csvwriter_t *ctx, char *value);
int csvwriter_eol(csvwriter_t *ctx);
int csvwriter_close(csvwriter_t *ctx);

#endif // CSVWRITER_H_INCLUDED
