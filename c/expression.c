//
// Created by apalagin on 6/18/2017.
// Expression evaluation based on Duktape engine
//

#include <duktape.h>
#include "expression.h"

static duk_context *DUK_CTX = NULL;

parsed_value_t *evaluate_expression(char *expr, map_t *vars)
{
    // Init context lazily
    if (DUK_CTX == NULL)
        DUK_CTX = duk_create_heap_default();

    // Initializing variables
    char var_expr[255];
    map_iterator_t *it = map_iterator(vars);
    map_entry_t *e;
    parsed_value_t *val;
    for (e = it->head; e != NULL; e = e->next) {
        val = e->data;
        memset(var_expr, '\0', sizeof(var_expr));

        if (val->type == LONG) {
            sprintf(var_expr, "%s = %ld", e->key, val->as.integer);
        } else if (val->type == DOUBLE) {
            sprintf(var_expr, "%s = %f", e->key, val->as.number);
        } else {
            sprintf(var_expr, "%s = '%s'", e->key, val->as.text);
        }

        duk_push_string(DUK_CTX, var_expr);
        duk_eval_noresult(DUK_CTX);
    }
    map_iterator_free(it);

    // Evaluating expression
    duk_push_string(DUK_CTX, expr);
    duk_eval(DUK_CTX);

    // Extracting result
    parsed_value_t *result = malloc(sizeof(parsed_value_t));
    if (duk_is_boolean(DUK_CTX, -1)) {
        result->type = LONG;
        result->as.integer = duk_get_boolean(DUK_CTX, -1);
    } else if (duk_is_number(DUK_CTX, -1)) {
        result->type = DOUBLE;
        result->as.number = duk_get_number(DUK_CTX, -1);
    } else {
        result->type = TEXT;
        strcpy(result->as.text, duk_get_string(DUK_CTX, -1));
    }

    duk_pop(DUK_CTX);
    return result;
}

