#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED

#include <time.h>

// Known data types
typedef enum type {
    TEXT,
    LONG,
    DOUBLE,
    DATE
} type_t;

// Input rule
typedef struct input input_t;
struct input {
    char field[255];
    char alias[255];
    
    type_t type;
    char format[255];

    input_t *next;
};

// Output rule
typedef struct output {
    char field[255];

    type_t type;
    char format[255];

    char action[255];

    char match_file[255];
    char match_column[255];
    char read_column[255];

    char expression[1024];
} output_t;

// Rule
typedef struct rule rule_t;
struct rule {
    input_t *inputs_head;
    int inputs_len;

    output_t *output;

    rule_t *next;
};

typedef struct filter {
    char expression[1024];
    input_t *inputs_head;
} filter_t;


// Configuration itself
typedef struct config {
    char encoding[20];
    char separator;
    filter_t *filter;
    rule_t *rules_head;
} config_t;

typedef struct tm timestamp_t;

typedef struct parsed_value {
    type_t type;
    union as {
        long integer;
        double number;
        timestamp_t date;
        char text[255];
    } as;
} parsed_value_t;

#endif // TYPES_H_INCLUDED
