//
// Created by Palagin, Alexey on 5/8/18.
//

#ifndef CPP_RULE_H
#define CPP_RULE_H

#include <string>
#include <vector>
#include <map>
#include "Input.h"
#include "Output.h"

using namespace std;

class Rule {
public:
    string apply(map<string, int> header, vector<string> row);

private:
    vector<Input> inputs;
    Output output;

    string extractValue(map<string, int> header, vector<string> row);

    string readValue(map<string, int> map, vector<string> vector, string basic_string);

    string evaluateExpression(map<string, int> header, vector<string> row);
};


#endif //CPP_RULE_H
