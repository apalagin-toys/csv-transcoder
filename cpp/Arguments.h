//
// Created by apalagin on 11/29/2017.
//

#ifndef CPP_ARGUMENTS_H
#define CPP_ARGUMENTS_H

#include <string>

class Arguments {

private:
    std::string _config;
    std::string _input;

public:
    Arguments(int i, char **pString);

    std::string config() { return _config; }
    std::string input()  { return _input;  }
};


#endif //CPP_ARGUMENTS_H
