//
// Created by apalagin on 11/29/2017.
//

#ifndef CPP_CSVTRANSCODER_H
#define CPP_CSVTRANSCODER_H


#include <fstream>
#include <map>
#include "XMLConfig.h"

class CsvTranscoder {
public:
    CsvTranscoder(const XMLConfig& config);

    void run(std::ifstream& ifstream);

private:
    XMLConfig config;
    std::map<std::string,int> headers;

    bool readHeader(const vector <string> &row);
};


#endif //CPP_CSVTRANSCODER_H
