//
// Created by apalagin on 11/29/2017.
//

#include "CsvTranscoder.h"
#include "CsvWriter.h"
#include "Filter.h"
#include <aria-csv-parser/parser.h>

using namespace aria::csv;
using namespace std;

CsvTranscoder::CsvTranscoder(const XMLConfig& config) {

}

void CsvTranscoder::run(ifstream& path) {
    CsvWriter writer;
    // TODO: correctly configure parser
    CsvParser parser(path);

    Filter filter;
    for (auto& row : parser) {
        if (readHeader(row)) continue;

        if (filter.apply(headers, row)) continue;

        for (auto& rule: config.getRules()) {
            string value = rule.apply(headers, row);
            writer.write(value);
        }

        writer.endLine();
    }
}

bool CsvTranscoder::readHeader(const vector<string> &row) {
    return false;
}
