//
// Created by apalagin on 11/29/2017.
//

#include "Arguments.h"
#include <tclap/CmdLine.h>

Arguments::Arguments(int i, char **pString) {
    try {
        TCLAP::CmdLine cmd("Csv Transcoder in C++", ' ', "0.1");
        TCLAP::ValueArg<std::string> configArg("c", "config", "XML configuration for transcoder", true, "config.xml", "path", cmd);
        TCLAP::UnlabeledValueArg<std::string> inputArg("i", "file to parse", true, nullptr, "path", cmd);

        cmd.parse(i, pString);

        this->_config = configArg.getValue();
        this->_input = inputArg.getValue();
    }
    catch (TCLAP::ArgException &e) {
        std::cerr << "Error: " << e.error() << " for arg " << e.argId() << std::endl;
    }
}
