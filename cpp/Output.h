//
// Created by Palagin, Alexey on 5/19/18.
//

#ifndef CPP_OUTPUT_H
#define CPP_OUTPUT_H


#include "Input.h"

class Output: Input {
public:
    string getExpression() {
        return expression;
    }

private:
    string expression;
};


#endif //CPP_OUTPUT_H
