//
// Created by Palagin, Alexey on 5/10/18.
//

#ifndef CPP_INPUT_H
#define CPP_INPUT_H

#include <string>
#include "Type.h"

using namespace std;

class Input {
public:
    string getField() { return field; }
    Type getType() { return type; }
    string getFormat() { return format; }

protected:
    string field;
    Type   type;
    string format;
};


#endif //CPP_INPUT_H
