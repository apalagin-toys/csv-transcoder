#include <iostream>
#include <fstream>
#include "Arguments.h"
#include "XMLConfig.h"
#include "CsvTranscoder.h"

int main(int argc, char** argv) {
    std::cout << "CSV Transcoder 0.1 [C++]" << std::endl;

    // Parse arguments
    Arguments args(argc, argv);

    // Parse XML configuration
    XMLConfig config(args.config());

    // Instantiate transcoder
    CsvTranscoder transcoder(config);

    // Run it
    std::ifstream inputFile(args.input());
    transcoder.run(inputFile);

    return 0;
}