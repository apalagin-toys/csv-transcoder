//
// Created by Palagin, Alexey on 5/8/18.
//

#include "Rule.h"

string Rule::apply(map<string, int> header, vector<string> row) {
    if (output.getExpression() != nullptr || output.getExpression().size() > 0) {
        return evaluateExpression(header, row);
    }

    return extractValue(header, row);
}

string Rule::extractValue(map<string, int> header, vector<string> row) {
    Input& in = inputs[0];
    string src = readValue(header, row, in.getField());
    return typeConverter.convert(src, in.getType(), in.getFormat(), output.getType(), output.getFormat());
}

string Rule::readValue(map<string, int> header, vector<string> row, string field) {
    // FIXME: Checks and security
    int idx = header[field];
    return row[idx];
}

string Rule::evaluateExpression(map<string, int> header, vector<string> row) {
    // FIXME: Implement expression evaluation
    return std::string();
}
