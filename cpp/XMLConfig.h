//
// Created by apalagin on 11/29/2017.
//

#ifndef CPP_XMLCONFIG_H
#define CPP_XMLCONFIG_H


#include <string>
#include <altivec.h>
#include <vector>
#include "Rule.h"

class XMLConfig {
public:
    XMLConfig(const std::string& path);

    XMLConfig();

    std::vector<Rule> getRules();
};


#endif //CPP_XMLCONFIG_H
