//
// Created by Palagin, Alexey on 5/8/18.
//

#ifndef CPP_FILTER_H
#define CPP_FILTER_H


#include <string>
#include <vector>
#include <map>

using namespace std;

class Filter {
public:
    bool apply(map<string, int> headers, vector<string> row);
};


#endif //CPP_FILTER_H
