//
// Created by Palagin, Alexey on 5/8/18.
//

#ifndef CPP_CSVWRITER_H
#define CPP_CSVWRITER_H


#include <string>

class CsvWriter {
public:
    void write(std::string field);
    void endLine();
};


#endif //CPP_CSVWRITER_H
