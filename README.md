Event Logs Transcoding and Processing
======================================

This is a toy project to learn most popular programming languages using close to real life tasks.
The goal is experimenting with language features and not trying to get production-ready code covering 100% requirements.
In this project I'll be focusing on console-based applications only. Cross-platform code is not priority. And right now I am limited
with Windows 10 platform.

What is Event Logs Transcoding
---------------------------------------

This is a real feature for the business processes in Ad analytics company I've been working for. "Transcoding" means converting file from
initial (customer-specific) format to a standard well-defined CSV format understandable by upstream data pipe line. The process itself is a parser
which applies some set of rules against input data. There are a lot of different rules, but most common is just copying column values from input to output 
or extracting require values from external files by matching column values with there content. In the latter case external files are called "match tables". 
There are other rules which converts input value format, for example hashes input content, or evaluating expression based on column values, 
or calling external service to process input data in a more complex custom way.

Requirements / Specification
--------------------------------------

For simplicity we will be supporting only these list of features:
- Plain copy of input column value to output column value
- Date/Time conversion
- Hashing input values
- Match tables
- Expression evaluation based on input column values

Let's state that input configuration and rules are passed as XML file. Here is example with all features being used:

```xml
<csv encoding="UTF-8" separator=",">
  <!-- Filtering records -->
  <filter expression="cost > 0"> 	    
    <input field="DBM Total Media Cost (USD)" type="long" var="cost"/> 	
  </filter>
  <!-- Output rules -->
  <rules>
    <rule>
      <!-- Date/Time convertion -->
      <input field="Event Time" type="date" format="UNIX_TIMESTAMP_NS"/>
      <output field="event_time" type="date" format="yyyy-MM-dd hh:mm:ss"/>
    </rule>
    <!-- Plain copy -->
    <rule>
      <input field="Advertiser ID" type="long"/>
      <output field="advertiser_id" type="long"/>
    </rule>          
    <!-- Hashing -->
    <rule>
      <input field="User ID" type="string"/>
      <output field="uid_hash" type="long" action="hash"/>
    </rule>
    <!-- Matching DBM City ID to city name -->
    <rule>
      <input field="DBM City ID" type="long"/>
      <output field="city_name" match_file="cities.csv" match_column="dbm_city_id" read_column="city_name"/>
    <!-- Expression evaluation -->
    <rule>
      <input field="Total Media Cost (USD)" type="long" var="mc"/>
      <input field="Impressions" type="long" var="imps"/>
      <output field="media_cost_cpm" expr="mc / imps / 1000" type="long"/>
    </rule>     
  </rules>
</csv>
```

Programming Languages
---------------------------------------

Main stream (from TIOBE Top 20):
- Java
- C
- C++
- Python
- C#
- JavaScript
- Visual Basic
- PHP
- Ruby
- Go
- Objective-C
- Swift

Other cool languages (from TIOBE Top 50):
- Groovy
- Lua
- Rust
- Scala
- Kotlin
- Clojure
- Erlang
