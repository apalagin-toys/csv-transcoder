Java version
==================================

This pet project started from Groovy language using Intellij IDEA CE. Though I got stuck with implemenation
with performance issues and switched to Java. That's why Groovy and Java code may
look sometimes like a copy-paste. Code is designed using my current knowledge of Java SE 8.

Runtime and Libraries
-------------------------------

* Oracle Java 8
* Assembled by Apache Maven with Shade plugin
* dom4j for parsing XML configuration files
* Apache Common CSV for parsing/generating CSV files
* Java built-in JavaScript engine for expression evaluation

Performance
------------

| platform | java -version | Sample          | Time      | CPU | RAM  | 
| -------- | ------------- |---------------- | --------- | --- | ---- |
| Windows  | 1.8.0_131-b11 | dbm.log (352M)  | 0m40.325s | 20% | 750M |
| Windows  | 1.8.0_131-b11 | 1987.csv (122M) | 2m7.557s  | 23% | 786M |

Implementation notes
---------------------

This is not a "production-ready" code: it's missing validations and uses unsafe
operations.  But it's capable of parsing any production files I have seen.
The performance is not sky-high due to relatively slow parser. 

The memory consumption is also huge b/c all matching tables will be parsed into memory map. But then matching  
should be faster then production version which uses intermediate storage.

JavaScript is probably overkill for this exercise and will affect overall performance.
But for the first iteration it's the shortest and easiest way of implementing expressions.

