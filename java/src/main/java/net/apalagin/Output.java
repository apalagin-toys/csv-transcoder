package net.apalagin;

public class Output {
	
	private String field;
	private String type;
	private String format;
	private String action;
	private String expression;
	
	private String matchTable;
	private String matchColumn;
	private String readColumn;
	
	public String getField() {
		return field;
	}
	
	public void setField(String field) {
		this.field = field;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getFormat() {
		return format;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public String getExpression() {
		return expression;
	}
	
	public void setExpression(String expression) {
		this.expression = expression;
	}
	
	public String getMatchTable() {
		return matchTable;
	}

	public String getMatchColumn() {
		return matchColumn;
	}

	public String getReadColumn() {
		return readColumn;
	}

	public void setMatchTable(String matchTable) {
		this.matchTable = matchTable;
	}

	public void setMatchColumn(String matchColumn) {
		this.matchColumn = matchColumn;
	}

	public void setReadColumn(String readColumn) {
		this.readColumn = readColumn;
	}

	public boolean hasExpression() {
		return isNotBlank(expression);
	}
	
	public boolean hasAction() {
		return isNotBlank(action);
	}
	
	public boolean hasMatching() {
		return isNotBlank(matchTable);
	}
	
	private boolean isNotBlank(String s) {
		return s != null && s.length() > 0;
	}

	@Override
	public String toString() {
		return "Output [field=" + field + ", type=" + type + ", format=" + format + ", action=" + action
				+ ", expression=" + expression + ", matchTable=" + matchTable + ", matchColumn=" + matchColumn
				+ ", readColumn=" + readColumn + "]";
	}
}
