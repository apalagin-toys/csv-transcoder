package net.apalagin.expr;

import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

public class ExpressionEvaluator {
	
	private ScriptEngine evaluator = new ScriptEngineManager().getEngineByName("nashorn");
	private Bindings bindings = new SimpleBindings();
	
	public Object evaluate(String expression, Map<String, Object> inputData) throws Exception {
		for (Map.Entry<String, Object> e: inputData.entrySet()) {
			bindings.put(e.getKey(), e.getValue());
		}
		
		return evaluator.eval(expression, bindings);
	}
}
