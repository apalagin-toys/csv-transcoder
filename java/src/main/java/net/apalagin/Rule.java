package net.apalagin;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Rule {

	private final List<Input> inputs = new LinkedList<>();
	private final List<Output> outputs = new LinkedList<>();
	
	public List<Input> getInputs() {
		return Collections.unmodifiableList(inputs);
	}
	
	public List<Output> getOutputs() {
		return Collections.unmodifiableList(outputs);
	}
	
	public void addInput(Input in) {
		inputs.add(in);
	}
	
	public void addOutput(Output out) {
		outputs.add(out);
	}
}
