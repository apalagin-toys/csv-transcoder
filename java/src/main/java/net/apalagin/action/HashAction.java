package net.apalagin.action;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashAction implements Action {

	@Override
	public String apply(String input) {
		// Source: http://stackoverflow.com/questions/415953/how-can-i-generate-an-md5-hash
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException ex) {
			throw new RuntimeException(ex);
		}
		
		byte[] hash = md.digest(input.getBytes(Charset.forName("UTF-8")));
		return String.format("%032x", new BigInteger(1, hash));
	}
}
