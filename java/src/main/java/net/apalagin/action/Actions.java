package net.apalagin.action;

import java.util.HashMap;
import java.util.Map;

public class Actions {
	
	private static final Map<String, Action> ACTIONS = new HashMap<>();
	static {
		ACTIONS.put("hash", new HashAction());
	}
	
	public static Action get(String action) {
		return ACTIONS.get(action);
	}
}
