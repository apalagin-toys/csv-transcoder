package net.apalagin.action;

/**
 * Contract for calling action against input data
 * 
 * @author apalagin
 */
public interface Action {
	
	String apply(String input);
	
}
