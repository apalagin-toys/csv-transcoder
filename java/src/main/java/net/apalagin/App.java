
package net.apalagin;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) throws Exception {
    	Options options = buildOptions();
    	
    	CommandLine cl = null;
    	try {
    		CommandLineParser parser = new DefaultParser();
    		cl = parser.parse(options, args);
    		
    	} catch (ParseException pex) {
    		HelpFormatter help = new HelpFormatter();
    		help.printHelp("App", options);
    		System.exit(1);
    	}
    	
    	Configuration config = Configuration.fromXml(cl.getOptionValue('c'));
    		
        CsvTranscoder transcoder = new CsvTranscoder(config);
        transcoder.run(cl.getOptionValue('i'), cl.getOptionValue('o'));
    }
    
    private static Options buildOptions() {
    	Option config = Option.builder("c")
    			.longOpt("config")
    			.hasArg()
    			.argName("file")
    			.desc("configuration file")
    			.build();
    	
    	Option input = Option.builder("i")
    			.longOpt("input")
    			.hasArg()
    			.argName("FILE")
    			.desc("input CSV file")
    			.build();
    	
    	Option output = Option.builder("o")
    			.longOpt("output")
    			.hasArg()
    			.argName("FILE")
    			.desc("output CSV file")
    			.build();
    	
    	Options options = new Options();
    	options.addOption(config);
    	options.addOption(input);
    	options.addOption(output);
    	
    	return options;
    }
}
