package net.apalagin.type;

/**
 * Contract for converting parsed object to type <T>
 * 
 * @author apalagin
 *
 * @param <T>
 */
public interface Converter<T> {
	
	T convert(Object object, String format);
	
}
