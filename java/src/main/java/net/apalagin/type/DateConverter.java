package net.apalagin.type;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter implements Converter<Date> {

	@Override
	public Date convert(Object object, String format) {
		if (object instanceof String)
			return convert((String) object, format);

		if (object instanceof Long)
			return convert((Long) object, format);
		
		if (object instanceof Double)
			return convert((Double) object, format);
		
		if (object instanceof Date)
			return (Date) object;

		throw new IllegalArgumentException("Unknown object type: " + object.getClass().getSimpleName());
	}
	
	private Date convert(String value, String format) {
		if ("UNIX_TIMESTAMP_NS".equals(format)) {
			long ms = (long) (Long.valueOf(value) / 1e3);
			return new Date(ms);
		}
		
		DateFormat formatter = new SimpleDateFormat(format);
		try {
			return formatter.parse(value);
		} catch (ParseException pex) {
			throw new IllegalArgumentException(pex);
		}
	}

	private Date convert(Long value, String format) {
		return new Date(value);
	}

	private Date convert(Double value, String format) {
		return new Date(value.longValue());
	}
}
