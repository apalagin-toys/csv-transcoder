package net.apalagin.type;

public class DoubleConverter implements Converter<Double> {
	@Override
	public Double convert(Object o, String format) {
		if (o == null)
			return null;
		
		if (o instanceof Double) 
			return (Double) o;
		
		if (o instanceof String) 
			return Double.valueOf(o.toString());
		
		if (o instanceof Long)
			return new Double((Long) o);
		
		throw new IllegalArgumentException("Unknown type: " + o.getClass().getSimpleName());
	}
}


