package net.apalagin.type;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringConverter implements Converter<String> {

	@Override
	public String convert(Object object, String format) {
		if (object instanceof Date) {
			DateFormat df = new SimpleDateFormat(format);
			return df.format((Date) object);
		}
		
		if (object instanceof Double) {
			if (format == null)
				return object.toString();
			return String.format(format, object);
		}
		
		return object.toString();
	}
}
