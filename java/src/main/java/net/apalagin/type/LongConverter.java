package net.apalagin.type;

public class LongConverter implements Converter<Long> {
	@Override
	public Long convert(Object o, String format) {
		if (o == null)
			return 0L;
		
		if (o instanceof Long)
			return (Long) o;
		
		if (o instanceof Double)
			return ((Double) o).longValue();
		
		if (o instanceof String)
			return Long.valueOf(o.toString());
		
		throw new IllegalArgumentException("Unknown type: " + o.getClass().getSimpleName());
	}
}
