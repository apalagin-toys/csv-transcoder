package net.apalagin;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents CSV filter: if filter expression evaluates to false
 * then record must be skipped
 */
public class Filter {
    private String expression;
    private List<Input> inputs = new LinkedList<>();

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<Input> getInputs() {
        return Collections.unmodifiableList(inputs);
    }

    public void addInput(Input input) {
        this.inputs.add(input);
    }
}
