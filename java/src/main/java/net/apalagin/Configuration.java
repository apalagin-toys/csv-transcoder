package net.apalagin;

import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Configuration {
	
	private final String encoding;
	private final Character separator;
	private final Filter filter;
	private final List<Rule> rules;
	
	private Configuration(String encoding, Character separator, Filter filter, List<Rule> rules) {
		this.encoding = encoding;
		this.separator = separator;
		this.filter = filter;
		this.rules = rules;
	}
	
	public String getEncoding() {
		return encoding;
	}

	public Character getSeparator() {
		return separator;
	}

	public Filter getFilter() {
		return filter;
	}

	public List<Rule> getRules() {
		return Collections.unmodifiableList(rules);
	}

	public static Configuration fromXml(String filename) throws DocumentException {
		SAXReader saxReader = new SAXReader();
		Document document = saxReader.read(new File(filename));
		
		Element root = document.getRootElement();

		Filter filter = null;
		Element filterEl = root.element("filter");
		if (filterEl != null) {
			filter = new Filter();
			filter.setExpression(filterEl.attributeValue("expression"));
			for (Iterator<Element> elemIt = filterEl.elementIterator(); elemIt.hasNext();) {
				Element el = elemIt.next();
				if (el.getName().equals("input")) {
					Input input = new Input();
					input.setField(el.attributeValue("field"));
					input.setAlias(el.attributeValue("var"));
					filter.addInput(input);
				}
			}
		}
		
		List<Rule> rules = new LinkedList<>();
		for (Iterator<Element> it = root.element("rules").elementIterator(); it.hasNext();) {
			Element ruleEl = it.next();
			Rule rule = new Rule();
			
			for (Iterator<Element> ruleIt = ruleEl.elementIterator(); ruleIt.hasNext();) {
				Element el = ruleIt.next();
				
				if (el.getName().equals("input")) {
					Input input = new Input();
					input.setField(el.attributeValue("field"));
					input.setType(el.attributeValue("type"));
					input.setFormat(el.attributeValue("format"));
					input.setAlias(el.attributeValue("var"));
					rule.addInput(input);
				}
				
				if (el.getName().equals("output")) {
					Output output = new Output();
					output.setField(el.attributeValue("field"));
					output.setType(el.attributeValue("type"));
					output.setFormat(el.attributeValue("format"));
					output.setAction(el.attributeValue("action"));
					output.setExpression(el.attributeValue("expression"));
					output.setMatchTable(el.attributeValue("match_file"));
					output.setMatchColumn(el.attributeValue("match_column"));
					output.setReadColumn(el.attributeValue("read_column"));
					rule.addOutput(output);
				}
			}
			
			rules.add(rule);
		}
		
		return new Configuration(root.attributeValue("encoding"), root.attributeValue("separator").charAt(0), filter, rules);
	}
}
