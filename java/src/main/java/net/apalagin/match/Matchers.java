package net.apalagin.match;

import java.util.HashMap;
import java.util.Map;

public class Matchers {
	
	private final static Map<String, Matcher> MATCHERS = new HashMap<>();
	
	public static Matcher get(String matchTable, String matchColumn, String readColumn) throws Exception {
		String key = matchTable + matchColumn + readColumn;
		Matcher matcher = MATCHERS.get(key);
		if (matcher == null) {
			matcher = new Matcher(matchTable, matchColumn, readColumn);
			MATCHERS.put(key, matcher);
		}
		
		return matcher;
	}
}
