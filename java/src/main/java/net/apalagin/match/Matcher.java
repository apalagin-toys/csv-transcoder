package net.apalagin.match;

import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class Matcher {

	private final Map<String, String> matchTableMap = new HashMap<>();

	public Matcher(String matchTable, String matchColumn, String readColumn) throws Exception {
		try (Reader reader = new FileReader(matchTable)) {
			CSVParser parser = new CSVParser(reader, CSVFormat.RFC4180.withHeader());
			try {
				for (CSVRecord record : parser) {
					matchTableMap.put(record.get(matchColumn), record.get(readColumn));
				}
			} finally {
				parser.close();
			}
		}
	}
	
	public String match(String value) {
		return matchTableMap.get(value);
	}
}
