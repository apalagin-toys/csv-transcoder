package net.apalagin;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import net.apalagin.action.Action;
import net.apalagin.action.Actions;
import net.apalagin.expr.ExpressionEvaluator;
import net.apalagin.match.Matcher;
import net.apalagin.match.Matchers;
import net.apalagin.type.*;

public class CsvTranscoder {

	private final ExpressionEvaluator FILTER_EVAL = new ExpressionEvaluator();
	private final ExpressionEvaluator EXPRESSION_EVAL = new ExpressionEvaluator();
	
	private final Converter<String> STRING_CONVERTER = new StringConverter();
	private final Converter<Date> DATE_CONVERTER = new DateConverter();
	private final Converter<Long> LONG_CONVERTER = new LongConverter();
	private final Converter<Double> DOUBLE_CONVERTER = new DoubleConverter();
	
	private final Configuration config;
	private final Map<String, Converter<?>> converters;

	public CsvTranscoder(Configuration config) {
		this.config = config;
		
		this.converters = new HashMap<>();
		this.converters.put("string", STRING_CONVERTER);
		this.converters.put("date", DATE_CONVERTER);
		this.converters.put("long", LONG_CONVERTER);
		this.converters.put("double", DOUBLE_CONVERTER);
	}

	public void run(String inputFilename, String outputFilename) throws Exception {
		try (Writer writer = new FileWriter(outputFilename)) {
			
			CSVPrinter printer = new CSVPrinter(writer, CSVFormat.RFC4180);
			printHeader(printer);

			try (Reader reader = new FileReader(inputFilename)) {
				CSVParser parser = new CSVParser(reader, CSVFormat.RFC4180.withHeader());
				try {
					for (CSVRecord record : parser) {
						if (isNotFiltered(record)) {
							try {
								transcode(record, printer);
							} catch (Exception ex) {
								System.err.println("Record skipped: " + ex.getMessage());
								System.err.println(" -- " + record.toString());
							}
						}
					}
				} finally {
					parser.close();
				}
			}
		}
	}
	
	private void printHeader(CSVPrinter printer) throws Exception {
		for (Rule rule: config.getRules()) {
			for (Output output: rule.getOutputs()) {
				printer.print(output.getField());
			}
		}
		printer.println();
	}

	private boolean isNotFiltered(CSVRecord record) throws Exception {
		if (config.getFilter() == null)
			return true;

		Map<String, Object> vars = new HashMap<>();
		for (Input input: config.getFilter().getInputs()) {
			vars.put(input.getAlias(), record.get(input.getField()));
		}

		return (boolean) FILTER_EVAL.evaluate(config.getFilter().getExpression(), vars);
	}

	private void transcode(CSVRecord record, CSVPrinter printer) throws Exception {
		List<String> newRecord = new ArrayList<>();
		
		for (Rule rule : config.getRules()) {
			// Collect input
			Map<String, Object> inputData = new HashMap<>();
			for (Input input : rule.getInputs()) {
				Object value = extractData(record, input);
				inputData.put(input.getSafeAlias(), value);
			}

			// Generate output
			for (Output output : rule.getOutputs()) {
				String value = convertData(inputData, output);
				
				if (output.hasMatching()) {
					Matcher matcher = Matchers.get(output.getMatchTable(), output.getMatchColumn(), output.getReadColumn());
					value = matcher.match(value);
				}
				
				if (output.hasAction()) {
					Action action = Actions.get(output.getAction());
					value = action.apply(value);
				}
				
				newRecord.add(value);
			}
		}
		
		printer.printRecord(newRecord);
	}
	
	private Object extractData(CSVRecord record, Input input) {
		String in = record.get(input.getField());
		if (in.isEmpty())
			return null;

		Converter<?> converter = this.converters.get(input.getType());
		if (converter == null) 
			throw new RuntimeException("Unknown type: " + input.getType());
		
		return converter.convert(in,  input.getFormat());
	}
	
	private String convertData(Map<String, Object> inputData, Output output) throws Exception {
		if (inputData.isEmpty())
			return "";
		
		Object result = null;
		if (output.hasExpression()) {
			result = EXPRESSION_EVAL.evaluate(output.getExpression(), inputData);
		} else {
			Converter<?> converter = this.converters.get(output.getType());
			if (converter == null)
				throw new RuntimeException("Unknown type: " + output.getType() + ". Rule: " + output.toString());
			
			result = converter.convert(inputData.values().toArray()[0], output.getFormat());
		}
		
		return STRING_CONVERTER.convert(result, output.getFormat());
	}
}
