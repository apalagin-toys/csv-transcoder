package net.apalagin.action;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HashActionTest {
	@Test
	public void testHash() {
		Action action = new HashAction();
		String md5Hash = action.apply("Alex P");
		// MD5 generated here: http://www.md5.cz/
		assertEquals("1adbba04402dd87b88d7d614a5e20576", md5Hash);
	}
}
