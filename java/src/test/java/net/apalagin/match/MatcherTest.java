package net.apalagin.match;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class MatcherTest {
	@Test
	public void testMatcher() throws Exception {
		String matchTable = getClass().getClassLoader().getResource("test-match-table.csv").getFile();
		Matcher matcher = new Matcher(matchTable, "ID", "NAME");
		assertEquals("Alex Palagin", matcher.match("1"));
		assertEquals("Nadiia Poliakovska", matcher.match("2"));
		assertEquals("Michael Palagin", matcher.match("3"));
		assertNull(matcher.match("4"));
	}
}
