package net.apalagin;

import static org.junit.Assert.*;

import org.dom4j.DocumentException;
import org.junit.Test;

public class ConfigurationTest {
	@Test
	public void testFromXml() throws DocumentException {
		Configuration config = Configuration.fromXml("src/test/resources/test-config.xml");
		assertNotNull(config);
	}
}
