package net.apalagin;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Test;

public class CsvTranscoderTest {
	@Test
	public void testPositive() throws Exception {
		Configuration config = Configuration.fromXml("src/test/resources/test-config.xml");
		CsvTranscoder transcoder = new CsvTranscoder(config);
		transcoder.run("src/test/resources/test.csv", "test.csv");
		
		try (Reader reader = new FileReader("test.csv")) {
			CSVParser parser = new CSVParser(reader, CSVFormat.RFC4180.withHeader());
			try {
				List<CSVRecord> records = parser.getRecords();
				assertEquals(3, records.size());
			} finally {
				parser.close();
			}
		}
	}
}
