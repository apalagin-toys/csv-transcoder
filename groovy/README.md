Groovy version
===============

Groovy version was written before Java version in kinda scripting style.
But after having performance issues it was decided to implement all features in Java first.
So filtering, matching and actions were introduced in Java version and them migrated
to Groovy. Otherwise Java version follows Groovy design :)

Designed is based on knowledge borrowed from groovy-lang.org docs and 
"Groovy 2 Cookbook".

Environment and Libraries
-------------------------------

* Oracle Java 1.8
* Groovy 2.4.10
* Assembled by Gradle with Shadow plugin
* Built-in XmlSlurper for configuration parsing
* CSV parser in Groovy from https://github.com/xlson/groovycsv/
* Groovy via JSR-223 for expression evaluation


Performance
------------

| platform | java -version | groovy | Sample          | Time      | CPU | RAM  | 
| -------- | ------------- |------- | --------------- | --------- | --- | ---- |
| Windows  | 1.8.0_131-b11 | 2.4.10 | dbm.log (352M)  | 0m58.930s | 19% | 500M |
| Windows  | 1.8.0_131-b11 | 2.4.10 | 1987.csv (122M) | 3m9.129s  | 22% | 477M |


Implementation notes
---------------------

Implementation is very similar to Java version, but with some idiomatic Groovy constructions.
In general code is shorter. Dynamic typing allows to use approaches impossible in Java:
see Converter implementations where type casting or "instanceof" is unnecessary. 

"com.xlson.groovycsv.CsvParser" is an interesting example of Groovy usage. But it can't write
CSV and so for that standard file writer is used.

Actions usage is based on dynamic feature of the language: object."method_name"(). This allows to
have all actions right in Actions class.

Matching implementation is also based on Groovy feature: it utilizes dynamic method handling 
and creates matcher instance right during match call. This approach is artificial and not a best solution, 
though it shortens the code.

Expression evaluation is based on Groovy via JSR-223 (scripting engine). Initially GroovyShell was used directly
but that was too slow and heavy.

