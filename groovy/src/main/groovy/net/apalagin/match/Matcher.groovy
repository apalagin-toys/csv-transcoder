package net.apalagin.match

import com.xlson.groovycsv.CsvParser

/**
 * Created by apalagin on 12/26/2016.
 */
class Matcher {
    private table = [:]

    Matcher(String matchTable, String matchColumn, String readColumn) {
        new FileReader(matchTable).withReader { Reader reader ->
            def records = CsvParser.parseCsv(reader)
            for (def record: records) {
                table[record."${matchColumn}"] = record."${readColumn}"
            }
        }
    }

    String match(String value) {
        return table[value];
    }
}
