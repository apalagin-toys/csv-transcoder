package net.apalagin.match

/**
 * Created by apalagin on 12/26/2016.
 */
class Matchers {

    Matchers() {
        this.metaClass = new ExpandoMetaClass(Matchers, false, true)
        this.metaClass.initialize()
    }

    def methodMissing(String name, args) {
        if (!name.startsWith("match"))
            throw new NoSuchMethodException()

        Matcher matcher = new Matcher(args[1], args[2], args[3]);
        this.metaClass."$name" = { Object[] params ->
            matcher.match(params[0])
        }

        return matcher.match(args[0])
    }
}
