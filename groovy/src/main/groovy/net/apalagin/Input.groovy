package net.apalagin

/**
 * Describes single data input - CSV column.
 * Every column has its name and data type
 */
class Input {

    String fieldName
    String dataType
    String dataFormat

    String fieldAlias

}
