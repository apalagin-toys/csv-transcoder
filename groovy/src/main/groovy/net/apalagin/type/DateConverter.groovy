package net.apalagin.type

import java.text.SimpleDateFormat

/**
 * Created by apalagin on 10/4/15.
 */
class DateConverter implements Converter<Date> {

    private final BigDecimal _1000 = new BigDecimal(1000);

    @Override
    Date convert(String value, String format) {
        if (format == "UNIX_TIMESTAMP_NS") {
            return new Date(new BigDecimal(value).divide(_1000).longValue());
        } else {
            return new SimpleDateFormat(format).parse(value)
        }
    }

    @Override
    Date convert(Long value, String format) {
        return new Date(value)
    }

    @Override
    Date convert(Double value, String format) {
        return new Date(value.longValue())
    }

    @Override
    Date convert(Date value, String format) {
        return value
    }
}
