package net.apalagin.type

import java.text.SimpleDateFormat

/**
 * Created by apalagin on 8/14/15.
 */
class StringConverter implements Converter<String> {

    @Override
    String convert(String value, String format) {
        return value
    }

    @Override
    String convert(Long value, String format) {
        return value.toString()
    }

    @Override
    String convert(Double value, String format) {
        return value.toString()
    }

    @Override
    String convert(Date value, String format) {
        return new SimpleDateFormat(format).format(value)
    }
}
