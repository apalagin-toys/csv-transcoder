package net.apalagin.type

import java.util.Date

/**
 * Created by apalagin on 8/14/15.
 */
public class LongConverter implements Converter<Long> {
    @Override
    public Long convert(String value, String format) {
        return Long.parseLong(value)
    }

    @Override
    public Long convert(Long value, String format) {
        return value
    }

    @Override
    public Long convert(Double value, String format) {
        return value.longValue()
    }

    @Override
    public Long convert(Date value, String format) {
        return value.getTime()
    }
}
