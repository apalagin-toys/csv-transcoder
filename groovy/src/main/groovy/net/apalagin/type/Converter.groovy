package net.apalagin.type

import groovy.transform.TypeChecked

/**
 * Contract for converting elements
 */
@TypeChecked
interface Converter<E> {

    E convert(String value, String format);

    E convert(Long value, String format);

    E convert(Double value, String format);

    E convert(Date value, String format);
}