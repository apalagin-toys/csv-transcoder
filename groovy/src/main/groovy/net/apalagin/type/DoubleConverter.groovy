package net.apalagin.type

/**
 * Created by apalagin on 8/14/15.
 */
class DoubleConverter implements Converter<Double> {

    @Override
    Double convert(String value, String format) {
        return Double.parseDouble(value)
    }

    @Override
    Double convert(Long value, String format) {
        return new Double(value)
    }

    @Override
    Double convert(Double value, String format) {
        return value
    }

    @Override
    Double convert(Date value, String format) {
        return value.getTime()
    }
}
