package net.apalagin

/**
 * Conversion rule
 *
 * Every rule has input data and output data. <tt>inputs</tt> points
 * to actual CSV fields and describes there format. <tt>outputs</tt> holds
 * actual conversion rules describing to what format input data set should be
 * converted
 */
class Rule {

    List<Input> inputs = []
    List<Output> outputs = []

}
