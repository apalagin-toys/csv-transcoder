package net.apalagin

/**
 * Configuration holder which contains set of rules describing conversion
 * from one CSV format to another CSV format.
 *
 * The main goal is abstraction from actual configuration file which can be easily
 * interpreted by other code parts.
 *
 *
 * <rules>
 <rule>
 <input field=”DATE” type=”date” format=”mm/dd/yyyy”/>
 <output field=”date” type=”date” format=”yyyy-mm-dd”/>
 </rule>
 <rule>
 <input field=”ID” type=”long”/>
 <output field=”id” type=”long”/>
 </rule>
 <rule>
 <input field=”A” type=”float”/>
 <input field=”B” type=”float”/>
 <input field=”C” type=”float”/>
 <output field=”a_b_c” type=”long” expr=”$A+$B+C”/>
 </rule>
 </rules>
 *
 */
class Configuration {

    String encoding;
    String separator;

    Filter filter

    List<Rule> rules = [];

    static Configuration fromXML(String configFile) {
        Configuration result = new Configuration()

        def xml = new XmlSlurper().parse(configFile)

        result.encoding = xml.@encoding
        result.separator = xml.@separator

        if (xml.filter.size() > 0) {
            result.filter = new Filter()
            result.filter.expression = xml.filter.@expression
            xml.filter.input.each {
                Input input = new Input()
                input.fieldName = it.@field
                input.fieldAlias = it.@var
                input.dataType = it.@type
                result.filter.inputs << input
            }
        }

        xml.rules.rule.each {
            Rule rule = new Rule()

            it.input.each {
                Input input = new Input()
                input.fieldName = it.@field
                input.fieldAlias = it.@var
                input.dataType = it.@type
                input.dataFormat = it.@format
                rule.inputs << input
            }

            it.output.each {
                Output output = new Output()
                output.fieldName = it.@field
                output.dataType = it.@type
                output.dataFormat = it.@format
                output.expression = it.@expression
                output.action = it.@action
                output.matchTable = it.@match_file
                output.matchColumn = it.@match_column
                output.readColumn = it.@read_column
                rule.outputs << output
            }

            result.rules << rule
        }

        return result
    }

}
