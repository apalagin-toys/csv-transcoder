package net.apalagin

import net.apalagin.action.Actions
import net.apalagin.expr.ExpressionEvaluator
import net.apalagin.match.Matchers
import net.apalagin.type.Converter
import net.apalagin.type.DateConverter
import net.apalagin.type.LongConverter
import net.apalagin.type.StringConverter
import com.xlson.groovycsv.CsvParser

/**
 * Core class which implements all conversion logic.
 *
 * Reads conversion rules for the given XML configuration
 * and applies them to the input data (<tt>src</tt>). The result is written
 * to <tt>dst</tt>
 *
 * TODO: Groovify it!
 */
class CsvTranscoder {

    private final Actions ACTIONS = new Actions()
    private final Matchers MATCHERS = new Matchers()
    private final ExpressionEvaluator FILTER_EVAL = new ExpressionEvaluator()
    private final ExpressionEvaluator EXPRES_EVAL = new ExpressionEvaluator()

    private Configuration config
    private Map<String, Converter<?>> converters

    CsvTranscoder(Configuration xmlConfig) {
        this.config = xmlConfig

        this.converters = [:]
        this.converters["long"] = new LongConverter()
        this.converters["string"] = new StringConverter()
        this.converters["date"] = new DateConverter()
    }

    /**
     * Converts CSV data from <tt>src</tt> path to <tt>dst</tt> path
     *
     * @param src full path to input csv file
     * @param dst full path to the output file
     */
    def run(src, dst) {
        int cols = this.config.rules.sum({ Rule it -> it.outputs.size() })

        // Writing data to dst
        new File(dst).withWriter { writer ->
            // Write header
            int i = 0
            config.rules.each {
                // TODO: Replace with OpenCSV
                for (Output output : it.outputs) {
                    writer.write("${output.fieldName}")
                    if (i++ < cols - 1)
                        writer.write(',')
                }
            }
            writer.write('\n')

            // TODO: introduce helper closure
            new FileReader(src).withReader { Reader reader ->
				
				def records = CsvParser.parseCsv(reader)

                Filter filter = config.filter
                for (def record: records) {
                    // Filtering
                    if (filter != null) {
                        def inputValues = getInputValues(record, filter.inputs)
                        if (!FILTER_EVAL.evaluate(filter.expression, inputValues))
                            continue
                    }

                    try {
                        transcode(record, writer, cols)
                    } catch (ex) {
                        println("Record skipped: " + ex.getMessage())
                    }
                }
            }
        }
    }

    private transcode(record, writer, int cols) {
        // Converting line according to rules
        int i = 0
        for (Rule rule : config.rules) {
            def inputData = getInputValues(record, rule.inputs)

            for (Output output : rule.outputs) {
                def result = convertData(output, inputData)

                if (output.action) {
                    result = ACTIONS."${output.action}"(result)
                }

                if (output.matchTable) {
                    result = MATCHERS."match${output.fieldName}"(
                            result,
                            "${output.matchTable}",
                            "${output.matchColumn}",
                            "${output.readColumn}")
                }

                if (result != null)
                    writer.write(result as String)
                if (i++ < cols - 1)
                    writer.write(',')
            }
        }

        writer.write('\n')
    }

    private getInputValues(record, List<Input> inputs) {
        def result = [:]
        for (Input input : inputs) {
            def value = extractData(record, input)
            def key = input.fieldAlias != null ? input.fieldAlias : input.fieldName
            result[key] = value
        }
        return result
    }

    /**
     * Extracts field value and converts it using the specified data type
     * @param data
     * @param field
     * @param dataType
     * @return
     */
    private extractData(record, Input rule) {
        def value = record."$rule.fieldName"
        if (value == null || value == '')
            return null
        this.converters[rule.dataType].convert(value, rule.dataFormat)
    }

    /**
     * Converts the specified data set (extracted from CSV line) according
     * to the output rule.
     *
     * @param rule
     * @param data
     * @return
     */
    private def convertData(rule, data) {
        def value = null
        if (rule.expression.isEmpty()) {
            if (data.values()[0] == null)
                value = ''
            else
                value = converters[rule.dataType].convert(data.values()[0], rule.dataFormat)
        } else {
            value = EXPRES_EVAL.evaluate(rule.expression, data)
        }

        this.converters["string"].convert(value, rule.dataFormat)
    }
}
