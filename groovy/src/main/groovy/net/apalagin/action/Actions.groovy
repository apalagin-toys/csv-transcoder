package net.apalagin.action

import java.nio.charset.Charset
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Entry point for accessing "actions" - pre-compiled functions
 */
class Actions {
    def hash(String value) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }

        byte[] hash = md.digest(value.getBytes(Charset.forName("UTF-8")));
        return String.format("%032x", new BigInteger(1, hash));
    }
}
