package net.apalagin.expr

import javax.script.Bindings
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.SimpleBindings

/**
 * Evaluator based on JSR-223
 */
class ExpressionEvaluator {

    // NOTE: Initially I used GroovyShell. But that appeared to be very
    // heavy and memory consuming. Stackoverflow also mentioned that using
    // GroovyShell is inefficient and JSR-223 is much more lightweight

    private ScriptEngine evaluator = new ScriptEngineManager().getEngineByName("groovy")
    private Bindings bindings = new SimpleBindings()

    Object evaluate(String expression, Map inputData) throws Exception {
        for (Map.Entry<String, Object> e: inputData.entrySet()) {
            bindings.put(e.getKey(), e.getValue())
        }

        evaluator.eval(expression, bindings)
    }

    private static String makeSafeVarName(String name) {
        return name.replaceAll(" ", "_")
    }
}