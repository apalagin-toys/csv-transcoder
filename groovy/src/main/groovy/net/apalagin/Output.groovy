package net.apalagin

/**
 * Describes single output - CSV field.
 * Output is similar to input (especially if its a copy), but output can instruct
 * how to run input data using other sinple data type or more complex expression.
 *
 * If expression is not defined then Output is considered to be simple conversion rule
 * for a single field. Expression may require multiple input fields, but still should
 * produce single vale.
 */
class Output extends Input {

    String expression;
    String action;

    String matchTable
    String matchColumn
    String readColumn

}
