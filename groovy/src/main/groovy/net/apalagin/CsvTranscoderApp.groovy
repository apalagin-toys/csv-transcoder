package net.apalagin

/**
 * Main entry point
 *
 * @author apalagin
 */

class CsvTranscoderApp {
	static void main(String[] args) {
		def cli = new CliBuilder(usage: 'transcode [options] <source> <destination>')
		cli.config(args: 1, argName: 'config', 'configuration in XML format')

		def options = cli.parse(args)

		print "Reading XML configuration from  ${options.config} ... "
		def config = Configuration.fromXML(options.config)
		println 'Done!'

		def src = options.arguments()[0]
		def dst = options.arguments()[1]
		def csvConverter = new CsvTranscoder(config)

		print "Converting ${src} to ${dst}..."
		csvConverter.run(src, dst)
		println 'Done!'
	}
}
