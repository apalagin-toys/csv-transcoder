import net.apalagin.Configuration
import net.apalagin.CsvTranscoder

/**
 * Test convering all functionality of CsvTranscoder
 */

CsvTranscoder converter = new CsvTranscoder(Configuration.fromXML('../resources/test.xml'))
converter.run('../resources/test-input.csv', 'output.csv')
