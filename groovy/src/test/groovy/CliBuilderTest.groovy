/**
 * Created by apalagin on 7/23/15.
 */

def cli = new CliBuilder(usage: 'transcode [options] <source> <destination>')
cli.config(args: 1, argName: 'config', 'configuration in XML format')

def options = cli.parse(['-config', 'test.xml', 'dcm.log', 'ak.csv'])
assert options

println cli.usage()
assert options.config == 'test.xml'
assert options.arguments() == ['dcm.log', 'ak.csv']

