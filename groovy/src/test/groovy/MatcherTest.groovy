import net.apalagin.match.Matchers

/**
 * Sanity test for Matchers and Matcher
 */

Matchers matchers = new Matchers()
def result = matchers.matchTest("1012938", "../resources/test-match.csv", "dbm_city_id", "nsr_city_id")
assert result == "1157627905"
result = matchers.matchTest("1013520", "../resources/test-match.csv", "dbm_city_id", "nsr_city_id")
assert result == "1157627915"