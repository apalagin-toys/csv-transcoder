import net.apalagin.Configuration

/**
 * Created by apalagin on 9/3/15.
 */

Configuration configuration = Configuration.fromXML("../resources/test.xml")
assert configuration != null
assert configuration.filter != null
assert configuration.filter.inputs.size() == 1
assert configuration.rules.size() == 5